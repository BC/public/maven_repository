**Warning!**

This repository is no longer used and will be *deleted soon*!

Instead please use either http://linphone.org/snapshots/maven_repository/ or http://linphone.org/releases/maven_repository/.

# Introduction

Starting with linphone-android release 4.1 the SDK part of the application is generated as an AAR.
If there is no locally built AAR, Android Studio will download one from this repository.

# Branches and version number

AAR are generated by linphone-sdk repository and version name will be the result of the git describe command in this repository.
Then the AAR will be uploaded here, in the same branch as the linphone-sdk was, except for releases.

# Using this maven repository

Inside your app/build.gradle, add the following:
```
repositories {
    maven {
        // Switch to release for releases !
        url "https://gitlab.linphone.org/BC/public/maven_repository/raw/master"
    }
}
```

Then in your dependencies list, add linphone-android-sdk:
```
implementation "org.linphone:linphone-sdk-android:<version_number>"
```

You can use `+` to get the latest snapshot version or hardcode a version number you specifically want.

# How are the AAR created and uploaded

Here's the gradle tasks used to upload the AAR:
```
def gitVersion = new ByteArrayOutputStream()
def gitBranch = new ByteArrayOutputStream()

task getGitVersion {
    exec {
        commandLine 'git', 'describe', '--always'
        standardOutput = gitVersion
    }
    exec {
        commandLine 'git', 'name-rev', '--name-only', 'HEAD'
        standardOutput = gitBranch
    }
    doLast {
        def branchSplit = gitBranch.toString().trim().split('/')
        def splitLen = branchSplit.length
        if (splitLen == 4) {
            gitBranch = branchSplit[2] + '/' + branchSplit[3]
            println("Local repository seems to be in detached head state, using last 2 segments of Git branch: " + gitBranch.toString().trim())
        } else {
            println("Git branch: " + gitBranch.toString().trim())
        }
    }
}

uploadArchives {
    repositories {
        mavenDeployer {
            configuration = configurations.deployerJars
            repository(url: 'git:' + gitBranch.toString().trim() + '://git@gitlab.linphone.org:BC/public/maven_repository.git')
            pom.project {
                groupId 'org.linphone'
                artifactId 'linphone-sdk-android'
                version gitVersion.toString().trim()
            }
        }
    }
}
```

The upload of the AAR to the maven repository (hosted in our gitlab) requires the following dependency: `maven { url "https://raw.github.com/synergian/wagon-git/releases"}`
